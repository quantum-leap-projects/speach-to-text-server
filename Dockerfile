FROM ubuntu:jammy-20220815 AS environment
# setup host system
RUN apt-get update
RUN apt install -y git ffmpeg python3-pip

# setup python environment
FROM environment AS base
COPY ./requirement.txt /
RUN pip3 install -r requirement.txt
RUN pip3 install git+https://github.com/openai/whisper.git

FROM base AS sst-server

COPY ./server/ /
COPY templates/ /templates
COPY static/ /static

CMD python3 server.py



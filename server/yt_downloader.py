import os
import urllib.parse
from pathlib import Path

import yt_dlp as youtube_dl


def download_mp3(url: str) -> Path:
    ydl_opts = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
        'outtmpl': 'temp.%(ext)s',
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        info_dict = ydl.extract_info(url, download=True)
        # Get the downloaded file path
        ydl.prepare_filename(info_dict)
        # Rename the downloaded file with the title of the video
        resultfile_name = "temp.mp3"
        # Return the new file name
        return Path(resultfile_name).absolute()


def main():
    global mp3_file
    youtube_url = "https://www.youtube.com/watch?v=jn8o9nTABWM"  # Replace with your YouTube video URL
    try:
        mp3_file = download_mp3(youtube_url)
        print(f"MP3 file downloaded and saved as '{mp3_file}'")
    except Exception as e:
        print("Error:", str(e))

    # Delete any residuals of the download (the video file)
    # if mp3_file and os.path.exists(mp3_file):
    #    os.remove(mp3_file)


if __name__ == "__main__":
    main()

import uvicorn
import serverapi

if __name__ == '__main__':
    try:
        server = uvicorn.Server
        uvicorn.run(serverapi.app, host="0.0.0.0", port=8000, access_log=True, log_config="log_config.yml")
    except(KeyboardInterrupt):
        uvicorn.Server.handle_exit()

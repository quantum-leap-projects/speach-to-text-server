import logging
import asyncio
import yt_downloader as ytd
import whisper
from fastapi import FastAPI, UploadFile, File, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

log = logging.getLogger(name='console')
model = whisper.load_model("large")
result = '{}'

app = FastAPI()

# Templates for rendering HTML
# local usage
# app.mount("/static", StaticFiles(directory="/home/tom/PycharmProjects/speach-to-text-server/static"), name="static")
# templates = Jinja2Templates(directory="../templates")
# container usage
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


# Serve the main webpage
@app.get("/", response_class=HTMLResponse)
async def read_root(request: Request):
    log.info("Loading web page")
    return templates.TemplateResponse("index.html", {"request": request})


@app.post("/upload")
async def upload(file: UploadFile = File(...)):
    try:
        log.info("Start upload processing")
        file_filename = await save_to_disk(file)
        asyncio.create_task(transcribe(file_filename))
        return {"message": "Data processing started."}
    except Exception as e:
        log.error("There was an error uploading the file: {}", e)
        return {"message": "There was an error uploading the file."}
    finally:
        file.file.close()


@app.post("/from-url")
async def from_url(link: str):
    log.info(link)
    try:
        mp3_file = ytd.download_mp3(link)
        log.info(mp3_file.absolute())
        if mp3_file.is_file():
            asyncio.create_task(transcribe(str(mp3_file)))
            return {"message": "Data processing started."}
    except Exception as e:
        log.error("There was an error processing the url:", e)
        return {"message": "Error."}


@app.get("/result")
async def load_result():
    global result
    return result


async def transcribe(input_data):
    global result
    result = await call_whisper(input_data)


async def save_to_disk(file):
    contents = file.file.read()
    file_filename = file.filename
    with open(file_filename, 'wb') as f:
        f.write(contents)
    return file_filename


async def call_whisper(filename: str):
    logging.info("Starting whisper")
    # load audio and pad/trim it to fit 30 seconds
    logging.info(f"Loading audio file: {filename}")
    # audio = whisper.load_audio(filename)
    result = model.transcribe(filename)
    text = result["text"]
    logging.info(f"Result: {text}")
    return text

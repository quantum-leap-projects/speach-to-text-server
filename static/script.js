// script.js
const uploadButton = document.getElementById('uploadButton');
const fileInput = document.getElementById('fileInput');
const urlInput = document.getElementById('urlInput');
const resultsDiv = document.getElementById('results');

uploadButton.addEventListener('click', () => {
    if (fileInput.files[0]) {
        body = fileInput.files[0];
        url = '/upload'
        fileUpload(url, body);
    } else if (urlInput.value) {
        body = urlInput.value.toString();
        url = '/from-url?link='
        urlRequest(url, body);
    }


});


function fileUpload(url, body){
    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data'
        },
        body: body,

    })
    .then(response => response.json())
    .then(data => {
        // Display uploaded result
        resultsDiv.innerHTML = JSON.stringify(data);
    })
    .catch(error => {
        console.error('Url request error:', error);
    });
}

function urlRequest(url, body){
    url = url+body
    fetch(url, {
        method: 'POST'
    })
    .then(response => response.json())
    .then(data => {
        // Display uploaded result
        resultsDiv.innerHTML = JSON.stringify(data);
    })
    .catch(error => {
        console.error('Upload error:', error);
    });
}

// Function to fetch results every 10 seconds
function fetchResults() {
    // Fetch results from your endpoint
    fetch('/result')
    .then(response => response.json())
    .then(data => {
        // Update results display
        resultsDiv.innerHTML = JSON.stringify(data);
    })
    .catch(error => {
        console.error('Results fetch error:', error);
    });
}

// Fetch results every 10 seconds
setInterval(fetchResults, 10000);
